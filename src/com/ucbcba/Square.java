package com.ucbcba;

public class Square implements Figure{

    int width;
    Square(int width) {    this.width = width;  }

    @Override
    public double perimeter() {
        return 4*width;
    }

    @Override
    public double area() {
        return width * width;//TODO
    }

    @Override
    public void draw() {
        System.out.println(perimeter());
        System.out.println(area());
        for (int i=0; i<width ;i++)
        {
            for(int j=0;j<width;j++)
            {
                System.out.print('*');
            }
            System.out.print("\n");
        }
        //TODO implementar draw de square

    }
}
