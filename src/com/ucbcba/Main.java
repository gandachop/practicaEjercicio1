package com.ucbcba;

public class Main {
    int propiedad;

    public static void main(String[] args) {
        Figure circle = new Circle(4);
        Figure square = new Square(4);
        draw(square);
        draw(circle);
    }

    public static void draw(Drawable drawable) {
        //TODO Imprimir si es un cuadrado o circulo
        if(drawable.getClass()==Circle.class)
        {
            System.out.println("es un circulo");
        }
        if(drawable.getClass()==Square.class)
        {
            System.out.println("es un cuadrado");
        }
        drawable.draw();
    }
}
