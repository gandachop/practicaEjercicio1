package com.ucbcba;

public class Circle implements Figure {
    double radius;
    Circle(double radius) {    this.radius = radius;  }


    @Override
    public double perimeter() {
        return 2*Math.PI*radius; //TODO
    }

    @Override
    public double area() {
        return Math.PI*radius*radius; //TODO
    }

    @Override
    public void draw() {
        System.out.println(perimeter());
        System.out.println(area());
        //TODO implementar draw de circle
    }
}
